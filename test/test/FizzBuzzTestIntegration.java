/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import controller.FizzBuzzController;
import java.io.IOException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import ws.FizzWebService;

/**
 *
 * @author David
 */
public class FizzBuzzTestIntegration {
    
    public FizzBuzzTestIntegration() {
    }
    
    @BeforeClass
    public static void headerTests() throws Exception{
        System.out.println("***********************************************"
                + "\n***********INTEGRATION TEST**************************"
        + "\n***************************************************************");
    }
    
    @AfterClass
    public static void afterTests() throws Exception{
        System.out.println("****************************************"
                + "******************FINAL TESTING****************"
                + "***************************************************");
    }
    
    /*@Before
    public void setUp(){
        System.out.println("start test");
    }
    
    @After
    public void afterTest(){
        System.out.println("final test");
    }*/
    
    @Test
    public void UserTestNumber() throws IOException{
        FizzWebService fws = new FizzWebService();
        
        int num = 2;
        ArrayList list = fws.fizzbuzz(num);
        if(num>0){
            assertTrue(true);
        }
        
        boolean isTrue = false;
        if(list != null){
            isTrue = true;
        }
        
        assertEquals(true, isTrue);
        
    }
    
    @Test
    public void fizz(){
        FizzBuzzController fbc = new FizzBuzzController();
        String res = fbc.fizzRes(3);
        assertEquals("de un divisible de 3 se espera el string fizz", "fizz", res);
    }

    @Test
    public void buzz(){
        FizzBuzzController fbc = new FizzBuzzController();
        String res = fbc.fizzRes(5);
        assertEquals("de un divisible de 5 se espera el string fizzbuzz", "buzz", res);
       
    }
    
    @Test
    public void fizzbuzz(){
        FizzBuzzController fbc = new FizzBuzzController();
        String res = fbc.fizzRes(15);
        assertEquals("de un divisible de 15 se espera el string fizzbuzz", "fizzbuzz", res);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import controller.FizzBuzzController;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author David
 */
public class FizzBuzzJUnitTest {
    
    
    public FizzBuzzJUnitTest() {
    }
    
    @Test
    public void fizzResTest(){
        FizzBuzzController fbc = new FizzBuzzController();
        
        String res = fbc.fizzRes(15);
        String res2 = fbc.fizzRes(3);
        String res3 = fbc.fizzRes(5);
        
        assertEquals("de un divisible de 15 se espera el string fizzbuzz", "fizzbuzz", res);
        assertEquals("de un divisible de 3 se espera el string fizz", "fizz", res2);
        assertEquals("de un divisible de 5 se espera el string buzz", "buzz", res3);
        
      
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import config.Configuration;
import exceptions.NoDivisible15;
import exceptions.NoDivisible3;
import java.util.ArrayList;
import exceptions.SmallerNumberException;

/**
 *
 * @author David
 */
public class FizzBuzzController implements Runnable{
    
    LoggerController lc = new LoggerController();
    
    private int start;
    private String res;
    
    static private String FIZZ = "fizz";
    static private String BUZZ = "buzz";
    static private String FIZZBUZZ = "fizzbuzz";

    public FizzBuzzController() {
    }
    
    
    public FizzBuzzController(int start){
        this.start = start;
    }
    
    public String getRes() {
        return res;
    }

    public void setRes(String res) {
        this.res = res;
    }
    
    
    @Override
    public void run(){
        lc.infoLog(Configuration.TipoLog.INFO, "Enter method run");
        ArrayList fizzbuzz = new ArrayList();
        int limit = Configuration.END;
        
        try {
            if(limit < start){
                throw new SmallerNumberException("Smaller number.");
            }
        } catch (SmallerNumberException e) {
            e.getMessage();
            lc.infoLog(Configuration.TipoLog.ERROR, "El número limite es más pequeño que el de start");
        }
        for (int i = start; i < limit; i++) {
            res = fizzRes(i);
            if(res == FIZZBUZZ){
                lc.infoLog(Configuration.TipoLog.DEBUG, res);
                fizzbuzz.add(FIZZBUZZ);
            }else if(res == FIZZ){
                lc.infoLog(Configuration.TipoLog.DEBUG, res);
                fizzbuzz.add(FIZZ);
            }else if(res == BUZZ){
                lc.infoLog(Configuration.TipoLog.DEBUG, res);
                 fizzbuzz.add(BUZZ);
            }else{
                lc.infoLog(Configuration.TipoLog.DEBUG, res);
                fizzbuzz.add(i);
                
            }
        }
        setRes(fizzbuzz.toString());
         lc.infoLog(Configuration.TipoLog.INFO, "Fin method run");
    }
    
    public String fizzRes(int i){
        
        lc.infoLog(Configuration.TipoLog.INFO, "Enter method fizzRe");
         
        try { 
                if(i%3 == 0 && i%5 == 0){
                    lc.infoLog(Configuration.TipoLog.DEBUG, "fizzbuzz: "+i);
                    return "fizzbuzz";
                }else if(i%3 == 0){
                    lc.infoLog(Configuration.TipoLog.DEBUG, "fizz: "+i);
                    return "fizz";
                }else if(i%5 == 0){
                    lc.infoLog(Configuration.TipoLog.DEBUG, "buzz: "+i);
                    return "buzz";
                }
            
        } catch (Exception e) {
            e.getMessage();
            lc.infoLog(Configuration.TipoLog.ERROR, e.getMessage());
        }
        lc.infoLog(Configuration.TipoLog.INFO, "Fin method fizzRe");
        return null;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import config.Configuration;
import config.Configuration.TipoLog;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


/**
 *
 * @author David
 */
public class LoggerController {
    private static Logger log = Logger.getLogger(LoggerController.class);
    
    public static void infoLog(TipoLog level, String message){
        Properties p = new Properties();
        try {
            p.load(new FileInputStream(Configuration.filePropsRoute));
        } catch (FileNotFoundException ex) {
            java.util.logging.Logger.getLogger(LoggerController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(LoggerController.class.getName()).log(Level.SEVERE, null, ex);
        }
        PropertyConfigurator.configure(p);
        
        switch(level){
            case DEBUG:
                System.out.println("debug"+ log);
                log.debug(message);
                    break;
            case ERROR:
                log.error(message);
                     break;
            case INFO:
                log.info(message);
                    break;
            case WARNING:
                log.warn(message);
        }
    }
    
}

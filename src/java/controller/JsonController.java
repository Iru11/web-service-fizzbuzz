/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import config.Configuration;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author David
 */
public class JsonController {
   ArrayList stringList;
    Date date;
    
    LoggerController lc = new LoggerController();

    public JsonController() {
    }
    
    public JsonController(ArrayList stringList) {
        this.stringList = stringList;
       // this.date = new Date();
    }
    
    
   public void addToJson(ArrayList listString, String file){
       lc.infoLog(Configuration.TipoLog.INFO, "Enter method addToJson");
       lc.infoLog(Configuration.TipoLog.DEBUG,listString.toString());
       lc.infoLog(Configuration.TipoLog.DEBUG, file);
       
       
       Gson gson = new GsonBuilder().setPrettyPrinting().create();
       JsonController jsc = new JsonController(listString);
       File f = new File(file);
       lc.infoLog(Configuration.TipoLog.WARNING, f.getAbsolutePath());
       
       String s = "";
       String line;
       
       if(!f.exists()){
           try {
               f.createNewFile();
           } catch (IOException ex) {
               Logger.getLogger(JsonController.class.getName()).log(Level.SEVERE, null, ex);
               lc.infoLog(Configuration.TipoLog.ERROR, ex.getMessage());
           }
       }else{
           try {
               BufferedReader br = new BufferedReader(new FileReader(f));
               br.read();
               while((line = br.readLine()) != null){
                   s+=line;
               }
               lc.infoLog(Configuration.TipoLog.DEBUG, s);
           } catch (FileNotFoundException ex) {
               Logger.getLogger(JsonController.class.getName()).log(Level.SEVERE, null, ex);
               lc.infoLog(Configuration.TipoLog.ERROR, ex.getMessage());
           } catch (IOException ex) {
               Logger.getLogger(JsonController.class.getName()).log(Level.SEVERE, null, ex);
               lc.infoLog(Configuration.TipoLog.ERROR, ex.getMessage());
           }
       }
       
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(f))){
            lc.infoLog(Configuration.TipoLog.DEBUG, gson.toJson(jsc));
            bw.write(gson.toJson(jsc));
        } catch (IOException ex) {
            Logger.getLogger(JsonController.class.getName()).log(Level.SEVERE, null, ex);
            lc.infoLog(Configuration.TipoLog.ERROR, ex.getMessage());
        }
       lc.infoLog(Configuration.TipoLog.INFO, "Fin method addToJson");
   }
    
}

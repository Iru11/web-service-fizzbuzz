package ws;

import com.google.gson.JsonObject;
import config.Configuration;
import static config.Configuration.TipoLog.DEBUG;
import controller.FizzBuzzController;
import controller.JsonController;
import controller.LoggerController;
import exceptions.NegativeNumException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;


/**
 *
 * @author David
 */
@WebService(serviceName = "FizzWebService")
public class FizzWebService{    

    //private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FizzWebService.class);
    LoggerController lc = new LoggerController();

    @WebMethod(operationName = "fizzbuzz")
    public ArrayList fizzbuzz(@WebParam(name = "num") Integer n){
       lc.infoLog(Configuration.TipoLog.INFO, "Enter method fizzbuzz");
      
       
        JsonObject jo = new JsonObject();
        ArrayList list = new ArrayList();
        JsonController json = new JsonController();
        
        try {
            if(n == null){
                throw new NullPointerException();
            }
            else if(n <0){
                throw new NegativeNumException("This number is not valid. It has to be a positive number.");
            }
        } catch (NullPointerException e) {
            e.getMessage();
            lc.infoLog(Configuration.TipoLog.ERROR, "The number is null");
        } catch(NegativeNumException e){
            e.getMessage();
            lc.infoLog(Configuration.TipoLog.ERROR, "Negative number error");
        }
        
        FizzBuzzController fbc = new FizzBuzzController(n);
        
        for(int i = 0; i <100; i++){
            Thread t = new Thread(fbc);
            t.start();
            if(fbc.getRes() == null){
              
                try {
                    t.sleep(100);
                } catch (InterruptedException ex) {
                    Logger.getLogger(FizzWebService.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                i--;
            }else{
                jo.addProperty("string", fbc.getRes());
                jo.addProperty("date", ""+new Date());
                list.add(jo+"\n");
                
            }
            
        }
       
        //lc.infoLog(DEBUG, list.toString());
        json.addToJson(list, Configuration.fileJsonRoute);
        lc.infoLog(Configuration.TipoLog.INFO, "Fin method fizzbuzz");
        return list;
    }
    
}


package exceptions;

/**
 *
 * @author diruela
 */
public class NegativeNumException extends Exception{
    
    private String message;

    public NegativeNumException(String message) {
        super();
        this.message = message;
    }
    
}

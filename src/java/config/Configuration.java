package config;

/**
 *
 * @author David
 */
public class Configuration {
    
    public static final int END = 50;
    
    public static final String fileJsonRoute = "C://Users//David//Documents//NetBeansProjects//FizzBuzzAplication//fizzbuzz.json";
    public static final String filePropsRoute = "C://Users//David//Documents//NetBeansProjects//FizzBuzzAplication//src//java//resources//log4j.properties";
    public static final String filelogsRoute = "C://Users//David//Documents//NetBeansProjects//FizzBuzzAplication///logs//fizzbuzzlogs.log";
    
    public enum TipoLog {
        DEBUG, ERROR, INFO, WARNING
    }
    
}
